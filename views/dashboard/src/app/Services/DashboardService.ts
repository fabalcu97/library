import {Component, Injectable} from '@angular/core';

import {ModuleInterface} from '../Interfaces/Module';

@Injectable()
export class DashboardService{

	ModuleRegister: Function;

	constructor(){
		this.ModuleRegister = null;
	}

	setModuleRegister(fun: Function){
		this.ModuleRegister = fun;
	}
}