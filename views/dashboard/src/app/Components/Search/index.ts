import {Component, OnInit, Input} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {CardComponent} from '../Card/index';
import {BookInterface as Book} from '../../Interfaces/Book';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-paginate';

@Component({
	selector: 'search-books',
	styles: [require('./styles.styl').toString()],
	template: require('./template.jade')(),
	directives: [CardComponent, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService]
})

export class SearchBooksComponent implements OnInit{

	ResultBook: Book;
	searchResult: boolean;
	titleSuggestions: any[];
	authorSuggestions: any[];
	$http: Http;
	@Input()
	BookToSearch: any;

	constructor($http: Http){
		this.$http = $http;
		this.ResultBook = null;
		this.searchResult = false;
		this.titleSuggestions = null;
		this.authorSuggestions = null;
		this.BookToSearch = {
			title: '',
			author: '',
			// date: ''
		};
	}

	ngOnInit(){
	}

	/*suggestTitle(){
		let params: URLSearchParams = new URLSearchParams();
		params.set('title', this.BookToSearch.title);

		this.$http.get('/api/suggest-title', { search: params }).subscribe(
			(res) => {
				this.titleSuggestions = res.json();
			},
			(err) => {
				console.log(err);
			}
		);
	}*/

	/*suggestAuthor(){
		let params: URLSearchParams = new URLSearchParams();
		params.set('author', this.BookToSearch.author);

		this.$http.get('/api/suggest-author', { search: params }).subscribe(
			(res) => {
				this.authorSuggestions = res.json();
			},
			(err) => {
				console.log(err);
			}
		);
	}*/
	
	SearchBook(){
		var searchUrl: string;
		let params: URLSearchParams = new URLSearchParams();

		searchUrl = "/api/search-book";
		
		params.set('title', this.BookToSearch.title);
		params.set('author', this.BookToSearch.author);
		// params.set('date', this.BookToSearch.date);

		this.$http.get(searchUrl, { search: params }).subscribe(
			(res) => {
				this.ResultBook = res.json();
				if(this.ResultBook.length != 0){
					this.searchResult = true;
				}
				else{
					this.searchResult = false;
					alert('No hay coincidencias');
				}
			},
			(err) => {

			}
		);
	}


}
