import {Component, ElementRef} from '@angular/core';
import {Http} from '@angular/http';
import {CardComponent} from '../Card/index';
import {BookInterface as Book} from '../../Interfaces/Book';

@Component({
  selector: 'add-books',
  styles: [require('./styles.styl').toString()],
  template: require('./template.jade')(),
  directives: [CardComponent]
})

export class AddBooksComponent{
	today: any;
	book: Book;
	$http: Http;
	element: ElementRef;
	submitBookURL: string;
	imagePreview: boolean;

	constructor($http: Http, element: ElementRef){
		this.element = element;
		this.$http = $http;
		this.imagePreview = false;
		this.book = {
			title: '',
			author: '',
			publicationYear: '',
			edition: '',
			samples: ''	,
			cover: ''
		};
		this.submitBookURL = '/api/save-book';
		this.today = new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate();
		console.log(this.today);

	}

	changeListner(file) {
		this.imagePreview = true;
		var fileReader = new FileReader();
		var inputFile = document.getElementById("inputFile").files[0];
		fileReader.readAsDataURL(inputFile);

		fileReader.onload = (e) => {
			var preview = document.getElementById("preview");
			preview.src = e.target.result;
		}

    }

	submitForm(){
		var tmp: any;
		tmp = document.getElementById('submitform');
		tmp.submit();
	}

}
