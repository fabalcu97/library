import {Component, OnInit, Input} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {CardComponent} from '../Card/index';
import {BookInterface as Book} from '../../Interfaces/Book';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-paginate';

@Component({
	selector: 'list-books',
	styles: [require('./styles.styl').toString()],
	template: require('./template.jade')(),
	directives: [CardComponent, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService]
})

export class ListBooksComponent implements OnInit{

	books: Book[];
	currentBook: Book;
	$http: Http;
	page: number;
	@Input()
	filterBook: any;

	constructor($http: Http){
		this.$http = $http;
		this.books = [];
		this.currentBook = null;
		this.page = 1;
		this.filterBook = {
			filter: 'true',
			author: '',
			beginDate: '',
			endDate: '',
		};
	}

	ngOnInit(){
		this.listBooks(this.page);
	}
	
	setBook(book: any){
		this.currentBook = book;
	}

	FilterBooks(){
		var filterUrl: string;
		let params: URLSearchParams = new URLSearchParams();

		filterUrl = "/api/get-books";
		
		params.set('filter', this.filterBook.filter);
		params.set('author', this.filterBook.author);
		params.set('beginDate', this.filterBook.beginDate);
		params.set('endDate', this.filterBook.endDate);

		this.$http.get(filterUrl, { search: params }).subscribe(
			(res) => {
				this.books = res.json();
			},
			(err) => {
				console.log(err);
			}
		);
	}

	public listBooks(page){
		this.books = [];
		let params: URLSearchParams = new URLSearchParams();
		page = parseInt(page)-1;
		params.set('page', page);
		this.$http.get('/api/get-books', { search: params }).subscribe(
			(res) => {
				this.books = res.json();
			},
			(err) => {
				console.log(err);
			}
		)
	}

}
