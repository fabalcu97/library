import {Component, OnInit, Input} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {CardComponent} from '../Card/index';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-paginate';

@Component({
	selector: 'search-books',
	styles: [require('./styles.styl').toString()],
	template: require('./template.jade')(),
	directives: [CardComponent, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService]
})

export class EditBooksComponent implements OnInit{

	ResultBook: any;
	searchResult: boolean;
	$http: Http;
	@Input()
	BookToSearch: any;

	constructor($http: Http){
		this.$http = $http;
		this.ResultBook = null;
		this.searchResult = false;
		this.BookToSearch = {
			title: '',
			author: '',
		};
	}

	ngOnInit(){

	}

	changeListner(file) {
		var fileReader = new FileReader();
		var inputFile = document.getElementById("inputFile").files[0];
		fileReader.readAsDataURL(inputFile);

		fileReader.onload = (e) => {
			var preview = document.getElementById("preview");
			preview.src = e.target.result;
		}

    }
	
	searchBook(){
		var searchUrl: string;
		let params: URLSearchParams = new URLSearchParams();

		searchUrl = "/api/search-book";
		
		params.set('title', this.BookToSearch.title);
		params.set('author', this.BookToSearch.author);
		// params.set('date', this.BookToSearch.date);

		this.$http.get(searchUrl, { search: params }).subscribe(
			(res) => {
				this.ResultBook = res.json();
				if(this.ResultBook.length != 0){
					this.searchResult = true;
				}
				else{
					this.searchResult = false;
					alert('No hay coincidencias');
				}
			},
			(err) => {

			}
		);
	}

	submitForm(){
		var tmp: any;
		tmp = document.getElementById('submitform');
		tmp.submit();
	}

}
