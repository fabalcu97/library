import {Component} from '@angular/core';
import {DashboardService} from '../../Services/DashboardService';
import {ModuleInterface as Module} from '../../Interfaces/Module';

@Component({
  selector: 'navbar',
  styles: [require('./styles.styl').toString()],
  template: require('./template.jade')(),
})

export class NavBarComponent{

	dashboard: DashboardService;
	modules: Module[];

	constructor(dashboard: DashboardService){
		this.dashboard = dashboard;
		this.modules = [];
		this.setupDashboardService();
	}

	setupDashboardService(){
		this.dashboard.setModuleRegister((module: Module) => {
			this.modules.push(module);
		})
	}

}
