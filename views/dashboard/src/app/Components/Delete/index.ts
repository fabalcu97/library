import {Component, OnInit, Input} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {CardComponent} from '../Card/index';
import {BookInterface as Book} from '../../Interfaces/Book';
import {PaginatePipe, PaginationControlsCmp, PaginationService} from 'ng2-paginate';

@Component({
	selector: 'list-books',
	styles: [require('./styles.styl').toString()],
	template: require('./template.jade')(),
	directives: [CardComponent, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [PaginationService]
})

export class DeleteBooksComponent implements OnInit{

	books: Book[];
	currentBook: Book;
	$http: Http;
	page: number;
	@Input()
	filterBook: any;

	constructor($http: Http){
		this.$http = $http;
		this.books = [];
		this.currentBook = null;
		this.page = 1;
		this.filterBook = {
			filter: 'true',
			title: '',
			author: '',
			beginDate: '',
			endDate: '',
		};
	}

	ngOnInit(){
		this.listBooks(this.page);
	}
	
	setBook(book: any){
		this.currentBook = book;
	}

	deleteBook(){
		var deleteUrl: string;
		deleteUrl = '/api/delete-book';

		let params: URLSearchParams = new URLSearchParams();
		
		params.set('title', this.currentBook.title);
		params.set('author', this.currentBook.author);
		params.set('date', this.currentBook.publicationYear);
		params.set('edition', this.currentBook.edition);
		params.set('samples', this.currentBook.samples);
		params.set('cover', this.currentBook.cover);

		this.$http.delete(deleteUrl, {search: params }).subscribe(
			(res) => {
				this.currentBook = null;
				this.listBooks(this.page);
			},
			(err) => {
				console.log(err);
			}
		);
	}

	FilterBooks(){
		var filterUrl: string;
		let params: URLSearchParams = new URLSearchParams();

		filterUrl = "/api/get-books";
		
		params.set('filter', this.filterBook.filter);
		params.set('title', this.filterBook.title);
		params.set('author', this.filterBook.author);
		params.set('beginDate', this.filterBook.beginDate);
		params.set('endDate', this.filterBook.endDate);

		this.$http.get(filterUrl, { search: params }).subscribe(
			(res) => {
				this.books = res.json();
			},
			(err) => {

			}
		);
	}

	public listBooks(page){
		this.books = [];
		page = parseInt(page)-1;
		this.$http.get('/api/get-books?page='+page).subscribe(
			(res) => {
				this.books = res.json();
			},
			(err) => {
				console.log(err);
			}
		)
	}

}
