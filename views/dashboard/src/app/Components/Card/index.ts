import {Component, Input} from '@angular/core';

@Component({
  selector: 'card',
  styles: [require('./styles.styl').toString()],
  template: require('./template.jade')(),
})

export class CardComponent{

	@Input()
	halfSizeWidth: boolean;
	@Input()
	halfSizeHeight: boolean;
	@Input()
	title: string;

	constructor(){

	}

}
