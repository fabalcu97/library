import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NavBarComponent} from './Components/Dashboard/index';
import {DashboardService} from './Services/DashboardService';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector : 'app',
  styles: [require('./styles.styl').toString()],
  template: require('./app.jade')(),
  directives: [NavBarComponent],
})
export class AppComponent implements OnInit{

	dashboard: DashboardService;
	router: Router;

	constructor(router: Router, dashboard: DashboardService, private cookieService: CookieService){
		this.router = router;
		this.dashboard = dashboard;
	}

	ngOnInit(){
		this.dashboard.ModuleRegister({
			name: 'Lista de Libros',
			action: () => { this.router.navigate(['list']); }
		});
		this.dashboard.ModuleRegister({
			name: 'Buscar libros',
			action: () => { this.router.navigate(['search']); }
		});
		this.dashboard.ModuleRegister({
			name: 'Agregar Libros',
			action: () => { this.router.navigate(['add']); }
		});
		this.dashboard.ModuleRegister({
			name: 'Editar Libros',
			action: () => { this.router.navigate(['edit']); }
		});
		this.dashboard.ModuleRegister({
			name: 'Eliminar Libros',
			action: () => { this.router.navigate(['delete']); }
		});
		this.dashboard.ModuleRegister({
			name: 'Cerrar Sesión',
			action: (req) => { 
				this.cookieService.removeAll();
				window.location.href = '/';
			}
		});
		
	}

}
