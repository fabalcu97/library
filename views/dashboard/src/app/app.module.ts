import {NgModule} from '@angular/core'
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app";
import {DashboardService} from './Services/DashboardService';
import {SafePipe} from './Pipes/SafeUrl';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@NgModule({
	declarations: [AppComponent, SafePipe],
	imports     : [BrowserModule, FormsModule, HttpModule, RouterModule.forRoot(rootRouterConfig)],
	providers   : [CookieService, DashboardService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
	bootstrap   : [AppComponent]
})
export class AppModule {

}
