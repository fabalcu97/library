import {Routes} from '@angular/router';
import {Component} from '@angular/core';

// Components
	import { ListBooksComponent } from './Components/List/index';
	import { SearchBooksComponent } from './Components/Search/index';
	import {AddBooksComponent} from './Components/Add/index';
	import {DeleteBooksComponent} from './Components/Delete/index';
	import {EditBooksComponent} from './Components/Edit/index';	


export const rootRouterConfig: Routes = [
	{path: '', redirectTo: 'list', terminal: true},
  	{path: 'list', component: ListBooksComponent},
  	{path: 'search', component: SearchBooksComponent},
  	{path: 'add', component: AddBooksComponent},
	{path: 'edit', component: EditBooksComponent},
	{path: 'delete', component: DeleteBooksComponent}
];

