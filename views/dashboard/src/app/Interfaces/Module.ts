export interface ModuleInterface{
	name: string;
	action: Function;
}