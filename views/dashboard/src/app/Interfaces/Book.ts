export interface BookInterface{
	title: string;
	author: string;
	publicationYear: string;
	edition: string;
	samples: string;
	cover: string;
}