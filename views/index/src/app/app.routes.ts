import {Routes} from '@angular/router';
import {Component} from '@angular/core';

// Components
	import {LoginComponent} from './Components/Login/index';
	import {RegisterComponent} from './Components/Register/index';

export const rootRouterConfig: Routes = [
	{path: '', redirectTo: 'login', terminal: true},
	{path: 'login', component: LoginComponent},
	{path: 'register', component: RegisterComponent},
];
