import {Component} from '@angular/core';

@Component({
  selector   : 'app',
  template: require('./app.jade')(),
  styles: [require('./styles.styl').toString()]
})
export class AppComponent {

}
