import {Component} from '@angular/core';
import { Http, Response } from '@angular/http';
import {CardComponent} from '../Card/index';

@Component({
	selector   : 'login',
	styles: [require('./styles.styl').toString()],
	template: require('./template.jade')(),
})

export class LoginComponent {

	// Variables
	$http: Http;
	auth: {
		user: string;
		password: string;
	};
	accessUrl: string;

	constructor($http: Http){
		this.$http = $http;
		this.accessUrl = 'api/auth';
		this.auth = {
			user: '',
			password: ''
		}
	}

	submit(){
		this.$http.post(this.accessUrl, this.auth).subscribe(
			(res) => {
				if( res.headers.toJSON()['access'] == 'granted' ){
					window.location.href ='/dashboard/';
				}
				else{
					alert("Usuario o contraseña incorrecto");
				}
			},
			(err) => {
				console.log(err);
			}
		)
	}

}
