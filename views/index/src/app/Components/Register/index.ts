import {Component} from '@angular/core';
import { Http, Response } from '@angular/http';

@Component({
  selector   : 'register',
  styles: [require('./styles.styl').toString()],
  template: require('./template.jade')(),
})

export class RegisterComponent {

	// Variables
	$http: Http;
	RegisterInfo: {
		name: string;
		email: string;
		username: string;
		password: string;
	};
	registerUrl: string;

	constructor($http: Http){
		this.$http = $http;
		this.registerUrl = 'api/register';
		this.RegisterInfo = {
			name: '',
			email: '',
			username: '',
			password: ''
		}
	}

	submit(){
		this.$http.post(this.registerUrl, this.RegisterInfo).subscribe(
			(resp) => {
				window.location.href='/dashboard/';
			},
			(err) => {}
		)
	}

}
