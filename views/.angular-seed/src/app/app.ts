import {Component} from '@angular/core';

@Component({
  selector   : 'app',
  template: require('./app.jade')(),
})
export class AppComponent {
}
