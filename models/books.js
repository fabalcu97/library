const pg = require('pg');
var dbConfig = require('../config/database');

const client = new pg.Client(conn);
client.connect();

query = client.query(
	`
		-- DROP TABLE books;
		CREATE TABLE books (
			id SERIAL NOT NULL PRIMARY KEY,
			title TEXT NOT NULL,
			author TEXT NOT NULL,
			publication_year DATE NOT NULL,
			edition TEXT NOT NULL,
			cover_image_link TEXT NOT NULL,
			samples INT NOT NULL
		);
	`
	);

query.on('end', () => {
	client.end();
});