const pg = require('pg');
var dbConfig = require('../config/database');

const client = new pg.Client(conn);
client.connect();

query = client.query(
	`
		-- DROP TABLE users;
		CREATE TABLE users (
			id SERIAL NOT NULL PRIMARY KEY,
			name TEXT NOT NULL,
			email TEXT NOT NULL,
			username TEXT NOT NULL,
			password TEXT NOT NULL
		);
	`
	);

query.on('end', () => {
	client.end();
});