var fs = require('fs');
var fse = require('fs-extra')
var path = require('path');
var express = require('express');
var router = express.Router();
var multer  = require('multer');
const pg = require('pg');

var dbConfig = require('../config/database');

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, './covers/');
	},
	filename: function (req, file, cb) {
		cb(null, req.body.title.replace(/ /g, '-').toLowerCase() + '.' + file.originalname.split('.')[1] );
	}
});

var upload = multer({ storage: storage }).fields(
	[
		{ name: 'cover', maxCount: 1 },
	]
);


// Grant access
router.post('/auth', (req, res, next) => {
	client = new pg.Client(conn);
	client.connect();

	var query = client.query({
		text: "SELECT * FROM users WHERE username = $1  AND password = $2",
		values: [req.body.user, req.body.password],
		name: 'User authentication'
	});
	
	query.on('row', (row) => {
		res.cookie('uid', row.id);
		res.cookie('username', row.username);
		res.cookie('name', row.name);
		res.cookie('email', row.email);
		res.setHeader('access', 'granted')
    });
	
	query.on('end', () => {
		client.end();
		res.end();
	});

});

router.post('/register', (req, res, next) => {
	client = new pg.Client(conn);
	client.connect();

	var query = client.query({
		text: "INSERT INTO users(name, email, username, password) VALUES($1, $2, $3, $4)",
		values: [req.body.name, req.body.email, req.body.username, req.body.password],
		name: 'User register'
	});

	query = client.query({
		text: "SELECT * FROM users WHERE username = $1  AND password = $2",
		values: [req.body.username, req.body.password]
	});
	
	query.on('row', (row) => {	
		res.cookie('uid', row.id);
		res.cookie('username', row.username);
		res.cookie('name', row.name);
		res.cookie('email', row.email);
		res.end();
    });
	
	query.on('end', () => {
		client.end();
	});

});

router.post('/save-book', (req, res, next) => {
	upload(req, res, (err) => {
		req.body.cover = req.files['cover'][0].filename;
		
		client = new pg.Client(conn);
		client.connect();

		var query = client.query({
			text: "INSERT INTO books(title, author, publication_year, edition, cover_image_link, samples) VALUES($1, $2, $3, $4, $5, $6)",
			values: [
				req.body.title,
				req.body.author,
				req.body.publicationYear,
				req.body.edition,
				req.body.cover,
				req.body.samples
			],
			name: 'Book register'
		});
		
		query.on('end', () => {
			client.end();
			res.redirect('/dashboard/');
		});
		
		if (err){
			console.log("Error => " + err);
		}
	})
});

router.get('/get-books', (req, res, next) => {
	client = new pg.Client(conn);
	client.connect();
	var Book = [];

	if(req.query.filter == "true"){

		var beginDate = new Date("January 01, 1500");
		var endDate = new Date("January 01, 3000");

		var query = client.query({
			text: `
				SELECT * FROM books
				WHERE title ~* $1
				AND author ~* $2
				AND (publication_year > $3 AND publication_year < $4)
				`,
			values: [
				req.query.title || '',
				req.query.author || '',
				req.query.beginDate || beginDate,
				req.query.endDate || endDate
			],
			name: 'Get filtered books'
		});
	}
	else{
		// console.log(req.query.page);
		// req.query.page = parseInt(req.query.page);
		var query = client.query({
			text: "SELECT * FROM books", // LIMIT 10 OFFSET $1",
			// values: [parseInt(req.query.page)*10],
			name: 'Get books'
		});
	}
	
	query.on('row', (row) => {
		Book.push({
			title: row.title,
			author: row.author,
			publicationYear: row.publication_year,
			samples: row.samples,
			edition: row.edition,
			cover: row.cover_image_link
		});
    });
	
	query.on('end', () => {
		client.end();
		res.json(Book);
		res.end();
	});
});

router.get('/search-book', (req, res, next) => {
	client = new pg.Client(conn);
	client.connect();
	var Book = [];

	var beginDate = new Date("January 01, 1500");

	var query = client.query({
		text: `
			SELECT * FROM books
			WHERE title ~* $1
			AND author ~* $2
			AND publication_year >= $3
			LIMIT 1`,
		values: [
			req.query.title || '',
			req.query.author || '',
			req.query.date || beginDate,
		],
		name: 'Get book'
	});
	
	query.on('row', (row) => {
		Book.push({
			id: row.id,
			title: row.title,
			author: row.author,
			publicationYear: row.publication_year,
			samples: row.samples,
			edition: row.edition,
			cover: row.cover_image_link
		});
    });
	
	query.on('end', () => {
		client.end();
		res.json(Book);
		res.end();
	});
});

router.post('/modify-book', (req, res, next) => {
	upload(req, res, (err) => {
		client = new pg.Client(conn);
		client.connect();
		if(req.files['cover']){
			req.body.lastCover = req.files['cover'][0].filename;
		}
		var query = client.query({
			text: `
				UPDATE books SET 
				(title, author, publication_year, edition, cover_image_link, samples) = 
				($1, $2, $3, $4, $5, $6)
				WHERE id = $7;
				`,
			values: [
				req.body.title,
				req.body.author,
				req.body.publicationYear,
				req.body.edition,
				req.body.lastCover,
				req.body.samples,
				req.body.id	
			],
			name: 'Modify book'
		});
		
		query.on('end', () => {
			client.end();
			res.redirect('/dashboard/');
		});

		if (err){
			console.log("Error => " + err);
		}

	});
});

router.delete('/delete-book', (req, res, next) => {
	client = new pg.Client(conn);
	client.connect();
	var Book = [];

	var query = client.query({
		text: `
			DELETE FROM books WHERE
			title = $1 AND
			author = $2 AND
			publication_year = $3 AND
			samples = $4 AND
			edition = $5 AND
			cover_image_link = $6
			`,
		values: [
			req.query.title,
			req.query.author,
			req.query.date,
			req.query.samples,
			req.query.edition,
			req.query.cover
		],
		name: 'Delete book'
	});
	
	query.on('end', () => {
		client.end();
		fse.remove(__dirname + '/../covers/' + req.query.cover, (err) => {
			if (err){
				return console.error(err);
			}
		});
		res.end();
	});
});


module.exports = router;
