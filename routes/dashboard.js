var express = require('express');
var router = express.Router();
var cookieParser = require('cookie-parser');

router.use('/dist', express.static(__dirname + '/../views/dashboard/src/dist'));

/* GET dashboard page. */
router.get('/', (req, res, next) => {
	if ( req.cookies.uid ){
		res.render('dashboard/src/index');
	}
	else{
		res.redirect('/');
	}
});

module.exports = router;
