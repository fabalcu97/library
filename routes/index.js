var express = require('express');
var router = express.Router();

router.use('/dist', express.static(__dirname + '/../views/index/src/dist'));

/* GET home page. */
router.get('/', (req, res, next) => {
	if ( req.cookies.uid ){
		res.redirect('/dashboard/');
	}
	else{
		res.render('index/src/index');
	}
});

module.exports = router;
